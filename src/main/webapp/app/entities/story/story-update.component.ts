import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IStory, Story } from 'app/shared/model/story.model';
import { StoryService } from './story.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';

@Component({
  selector: 'jhi-story-update',
  templateUrl: './story-update.component.html'
})
export class StoryUpdateComponent implements OnInit {
  isSaving = false;
  categories: ICategory[] = [];
  publishDateDp: any;

  editForm = this.fb.group({
    id: [],
    keyword: [],
    description: [],
    title: [],
    handle: [],
    content: [],
    author: [],
    isPublish: [],
    publishDate: [],
    category: []
  });

  constructor(
    protected storyService: StoryService,
    protected categoryService: CategoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ story }) => {
      this.updateForm(story);

      this.categoryService.query().subscribe((res: HttpResponse<ICategory[]>) => (this.categories = res.body || []));
    });
  }

  updateForm(story: IStory): void {
    this.editForm.patchValue({
      id: story.id,
      keyword: story.keyword,
      description: story.description,
      title: story.title,
      handle: story.handle,
      content: story.content,
      author: story.author,
      isPublish: story.isPublish === 1 ? true: false,
      publishDate: story.publishDate,
      category: story.category
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const story = this.createFromForm();
    if (story.id !== undefined) {
      this.subscribeToSaveResponse(this.storyService.update(story));
    } else {
      this.subscribeToSaveResponse(this.storyService.create(story));
    }
  }

  private createFromForm(): IStory {
    return {
      ...new Story(),
      id: this.editForm.get(['id'])!.value,
      keyword: this.editForm.get(['keyword'])!.value,
      description: this.editForm.get(['description'])!.value,
      title: this.editForm.get(['title'])!.value,
      handle: this.editForm.get(['handle'])!.value,
      content: this.editForm.get(['content'])!.value,
      author: this.editForm.get(['author'])!.value,
      isPublish: this.editForm.get(['isPublish'])!.value === true ? 1:0,
      publishDate: this.editForm.get(['publishDate'])!.value,
      category: this.editForm.get(['category'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICategory): any {
    return item.id;
  }
}
