import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IStory } from 'app/shared/model/story.model';

type EntityResponseType = HttpResponse<IStory>;
type EntityArrayResponseType = HttpResponse<IStory[]>;

@Injectable({ providedIn: 'root' })
export class StoryService {
  public resourceUrl = SERVER_API_URL + 'api/stories';

  constructor(protected http: HttpClient) {}

  create(story: IStory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(story);
    return this.http
      .post<IStory>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(story: IStory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(story);
    return this.http
      .put<IStory>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IStory>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStory[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(story: IStory): IStory {
    const copy: IStory = Object.assign({}, story, {
      publishDate: story.publishDate && story.publishDate.isValid() ? story.publishDate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.publishDate = res.body.publishDate ? moment(res.body.publishDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((story: IStory) => {
        story.publishDate = story.publishDate ? moment(story.publishDate) : undefined;
      });
    }
    return res;
  }
}
