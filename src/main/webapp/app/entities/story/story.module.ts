import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CrawlStorySharedModule } from 'app/shared/shared.module';
import { StoryComponent } from './story.component';
import { StoryDetailComponent } from './story-detail.component';
import { StoryUpdateComponent } from './story-update.component';
import { StoryDeleteDialogComponent } from './story-delete-dialog.component';
import { storyRoute } from './story.route';
import { CKEditorModule } from 'ckeditor4-angular';

@NgModule({
  imports: [CrawlStorySharedModule, RouterModule.forChild(storyRoute), CKEditorModule],
  declarations: [StoryComponent, StoryDetailComponent, StoryUpdateComponent, StoryDeleteDialogComponent],
  entryComponents: [StoryDeleteDialogComponent]
})
export class CrawlStoryStoryModule {}
