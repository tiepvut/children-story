import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.CrawlStoryCategoryModule)
      },
      {
        path: 'story',
        loadChildren: () => import('./story/story.module').then(m => m.CrawlStoryStoryModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class CrawlStoryEntityModule {}
