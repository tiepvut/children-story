import { Moment } from 'moment';
import { ICategory } from 'app/shared/model/category.model';

export interface IStory {
  id?: number;
  keyword?: string;
  description?: string;
  title?: string;
  handle?: string,
  content?: string;
  author?: string;
  isPublish?: number;
  publishDate?: Moment;
  category?: ICategory;
}

export class Story implements IStory {
  constructor(
    public id?: number,
    public keyword?: string,
    public description?: string,
    public title?: string,
    public handle?: string,
    public content?: string,
    public author?: string,
    public isPublish?: number,
    public publishDate?: Moment,
    public category?: ICategory
  ) {
    this.isPublish = this.isPublish || 0;
  }
}
