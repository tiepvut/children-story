export interface ICategory {
  id?: number;
  name?: string;
  handle?: string;
  display?: number;
}

export class Category implements ICategory {
  constructor(public id?: number, public name?: string, public handle?: string, public display?: number) {
    this.display = this.display || 0;
  }
}
