import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { CrawlStorySharedModule } from 'app/shared/shared.module';
import { CrawlStoryCoreModule } from 'app/core/core.module';
import { CrawlStoryAppRoutingModule } from './app-routing.module';
import { CrawlStoryHomeModule } from './home/home.module';
import { CrawlStoryEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    CrawlStorySharedModule,
    CrawlStoryCoreModule,
    CrawlStoryHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    CrawlStoryEntityModule,
    CrawlStoryAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class CrawlStoryAppModule {}
