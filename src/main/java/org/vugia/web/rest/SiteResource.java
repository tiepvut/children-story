package org.vugia.web.rest;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.HtmlUtils;
import org.vugia.domain.Category;
import org.vugia.domain.Story;
import org.vugia.repository.CategoryRepository;
import org.vugia.repository.StoryRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class SiteResource {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private StoryRepository storyRepository;

    private final Integer NUMBER_OF_STORIES_PER_PAGE = 15;

    private final Integer NUMBER_OF_STORIES_PER_CATEGORY = 10;

    @GetMapping(value = "/")
    public String showHomePage(Model model, HttpServletRequest request) {
        Pageable pageable = PageRequest.of(1, NUMBER_OF_STORIES_PER_CATEGORY);
        buildSessionValue(request);
        List<Category> categories = (List<Category>)request.getSession().getAttribute("menu");
        for(Category category: categories) {
            List<Story> stories = storyRepository.findByCategoryHandle(category.getHandle(), pageable).toList();
            category.setStories(stories);
        }
        model.addAttribute("categories", categories);
        return "index";
    }

    private void buildSessionValue(HttpServletRequest request) {
        Pageable pageable = PageRequest.of(1, NUMBER_OF_STORIES_PER_CATEGORY);
        if (request.getSession().getAttribute("top-hit") == null) {
            List<Story> stories = storyRepository.findAll(pageable).toList();
            request.getSession().setAttribute("top-hit", stories);
        }

        if (request.getSession().getAttribute("menu") == null) {
            List<Category> categories = categoryRepository.findPublishedCategory();
            request.getSession().setAttribute("menu", categories);
        }
    }

    @GetMapping(value = "/chuyen-muc/{category-handle}")
    public String getStoryOfCategory(Model model, @PathVariable("category-handle") String handle,
                                          @RequestParam(value = "page", required = false) Integer page,
                                     HttpServletRequest request) {
        buildSessionValue(request);
        int pageNum = 1;
        if (page != null) {
            pageNum = page;
        }
        Category category = categoryRepository.findCategoryByHandle(handle).get(0);
        Pageable pageable = PageRequest.of(pageNum, NUMBER_OF_STORIES_PER_PAGE);
        Page<Story> stories = storyRepository.findByCategoryHandle(handle, pageable);
        long totalRecords = stories.getTotalElements();
        model.addAttribute("categoryName", category.getName());
        model.addAttribute("stories", stories.toList());
        model.addAttribute("total", totalRecords);
        return "category";
    }

    @GetMapping(value = "/chuyen-muc/{category-handle}/{story-handle}")
    public String getStory(Model model, @PathVariable("story-handle") String handle,
                           HttpServletRequest request) {
        buildSessionValue(request);
        Story story = storyRepository.findByHandle(handle).get(0);
        String content = HtmlUtils.htmlUnescape(story.getContent());
        content = Jsoup.parse(content).text();
        story.setContent(content);
        model.addAttribute("category", story.getCategory());
        model.addAttribute("story", story);
        return "story";
    }
}
