/**
 * View Models used by Spring MVC REST controllers.
 */
package org.vugia.web.rest.vm;
