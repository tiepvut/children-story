package org.vugia.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.vugia.domain.Story;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Story entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoryRepository extends JpaRepository<Story, Long> {

    List<Story> findByLink(String link);
    List<Story> findByHandle(String handle);

    @Query(value = "SELECT story FROM Story story where story.category.handle = :categoryHandle ORDER BY story.id ASC")
    Page<Story> findByCategoryHandle(@Param("categoryHandle") String categoryHandle, Pageable pageable);
}
