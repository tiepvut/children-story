package org.vugia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.vugia.domain.Category;

import java.util.List;

/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = "SELECT category FROM  Category category WHERE category.display = 1")
    List<Category> findPublishedCategory();
    List<Category> findCategoryByHandle(String handle);
}
